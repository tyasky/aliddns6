# aliddns.sh

此脚本将指定的域名和当前设备的公网 IP 地址一起提交给域名解析服务器，使外网设备可以通过域名访问当前设备。

脚本同时支持 IPv4 和 IPv6。没有公网 IPv4 地址，运营商已支持 IPv6，以下就以 IPv6 为例。

## 1、测试是否已接入 IPv6 网络

[IPv6 测试](http://www.test-ipv6.com/)，成功接入 IPv6 网络显示如下：

![test-ipv6](./images/test-ipv6.png)

如果没接入 IPv6 网络，参考[中国电信IPv6地址获取教程](https://m.ithome.com/html/405571.htm)。

## 2、准备域名

去阿里云[万网](https://wanwang.aliyun.com/)购买一个域名。需要实名认证，购买域名时要填真实信息。

域名购买成功后，登录[阿里云](https://aliyun.com/)，进入控制台，[RAM 访问控制](https://ram.console.aliyun.com/overview)。用户 ➡️ 创建用户：

![createuser](./images/createuser.png)

图中勾选了**编程访问**，会生成 **AccessKey ID** 和 **AccessKey Secret**：

![idsecret](./images/idsecret.png)

为新加的用户添加权限 **AliyunDNSFullAccess**：

![dnsfullaccess](./images/dnsfullaccess.png)

## 3、下载脚本

直接在用脚本的设备运行

```shell
git clone https://gitee.com/tyasky/aliddns6
```

![2](./images/gitclone.png)

## 4、修改配置

修改配置文件 config.ini

![3](./images/config.ini.png)

## 5、手动执行

```shell
./aliddns6/aliddns.sh
```

## 6、自动运行

1. Windows

   若配置文件为脚本所在目录的 config.ini，双击 createSchTask.vbs，创建计划任务。

   其他情况，将配置文件拖放到 createSchTask.vbs 上，创建计划任务。
   
2. Linux

   创建定时任务

   ```shell
   crontab -e
   ```

   最后边添加如下内容：

   ```shell
   */5 * * * * /path/to/aliddns.sh -f /path/to/config.ini
   ```

   **命令中的路径都为绝对路径**。如上是每 5 分钟执行一次。

3. 华硕路由器梅林

   1. [安装 Entware](https://github.com/RMerl/asuswrt-merlin.ng/wiki/Entware)，然后安装 bash，gawk。

      ```shell
      opkg update && opkg install bash gawk
      ```

   2. 复制 aliddns.sh 到 /jffs/scripts/ 路径下，改名为 ddns-start。

      ```shell
      cp aliddns6/aliddns.sh /jffs/scripts/ddns-start
      ```

   3. 复制 config.ini 到 /jffs/scripts/ 路径下，修改配置。

      ```shell
      cp aliddns6/config.ini /jffs/scripts/
      ```

## 7、问题解决

1. 脚本报错，检查终端

   用命令 `whereis bash` 或 `echo $0` 确认系统中有没有 bash，没有则安装上。

   ![1](./images/1.png)
   
2. 手动删除解析记录

   主域名是 example.com，删除 test.example.com 的解析记录

   ```shell
   ~/aliddns6/aliddns.sh -f ~/config.ini -d test
   ```

## 8. 其他

1. [检查域名解析情况](https://zijian.aliyun.com/)。

2. [阿里云云解析 DNS API 文档](https://help.aliyun.com/document_detail/29738.html)。

3. 交流反馈扣扣群：585194793

